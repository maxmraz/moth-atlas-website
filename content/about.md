---
title: About
author: Max Mraz
---

<!-- ![Logo](moths_logo.gif) -->
<img align="left" src="/img/moths_logo.gif" style="width:20%; margin:1em;">
<br>

## I'm Max!

Moth Atlas is an indie game studio. We're about as indie as it gets. All our games so far have been developed solo by Max Mraz, doing the design, writing, code, music, graphics, and everything else. Now I'm teaching myself to make websites too so this page can exist, bear with me on its design since I'm learning!

Moth Atlas's first game was Ocean's Heart, created entirely by me as I taught myself to code. It was released on Steam in January 2021, with a Switch port released in January 2022. Between wrapping development of Ocean's Heart and its release, the 2D Bloodborne tribute game Yarntown was developed and released, to surprisingly large interest. Since then, Moth Atlas has developed some smaller projects, including Hallow's Eve and the Trillium project (still in development), which is a free and open-source set of music, graphic, and code assets to help anyone else who wants to make a game.

Moth Atlas is currently working on an unannounced Western Horror game.


## Contact:
Feel free to reach out to us on any of our social media channels:

<!-- {{ range .Site.Params.socialChannels }}
{{end}} -->
[<img src="/img/icons/twitter_logo.svg" class="social-link" alt="Twitter Link" />](https://twitter.com/MothAtlasGames)
[<img src="/img/icons/instagram_logo.svg" class="social-link" alt="Twitter Link" />](https://twitter.com/MothAtlasGames)
[<img src="/img/icons/reddit_logo.svg" class="social-link" alt="Twitter Link" />](https://twitter.com/MothAtlasGames)
[<img src="/img/icons/tiktok_logo.svg" class="social-link" alt="Twitter Link" />](https://twitter.com/MothAtlasGames)

If you'd like to send an email, you can reach me at:
Max@MothAtlas.co
