---
title: Yarntown
author: Max Mraz
splashImg: splash.png
itchioLink: https://maxatrillionator.itch.io/yarntown
screenshots: ['screen1.png', 'screen2.png', 'screen3.png', 'screen4.png']
---

Welcome to Yarntown, a Zeldalike tribute to Bloodborne. You’ve come on the night of the hunt. The streets of this cursed, gothic town are overrun with beasts, and what has become of the men who tried to hunt them before. Explore the twisting roads, go toe to toe with powerful foes, and uncover Yarntown’s dark secrets.


Yarntown is a 2D pixel art tribute to Bloodborne, a dark fantasy video game released on Playstation 4 in 2015. Bloodborne is as renowned for its demanding and merciless gameplay as for its incredible gothico-victorian atmosphere. This project was developed in one month and recreates Central Yharnam as a Zeldalike adventure.
