---
title: Tombwater
author: Max Mraz
splashImg: splash.png
itchioLink: https://maxatrillionator.itch.io/tombwater
trailerLink: https://www.youtube.com/embed/fQOO0oqbrUs
screenshots: ['screen1.gif', 'screen2.png', 'screen3.png', 'screen4.png', 'screen5.png', 'screen6.png']
summaryText: Explore a Wild West town, fallen to madness. Gun down the twisted eldritch horrors that walk its streets. Uncover the secrets that brought Tombwater to ruin.
---

Tombwater is currently in development, but try the demo at: https://maxatrillionator.itch.io/tombwater

## You ain't never leavin' Tombwater alive

Tombwater is a fast-paced action RPG set in the Wild West. Frantic action meets the exploration of a Zeldalike and the horror of unknowable beings lurking below the surface.

In Tombwater, you will piece together mysteries as you piece together whatever weapons, spells, and items you can scrounge together to defend yourself against the town's dangers. In Metroidvania style, you'll unravel the paths of this city as you collect new items and abilities. The varied effects of the town's eldritch influence will allow you to build unique strategies and playstyles. Whether you choose the life of a quick gunslinger, a maddened eldritch spellcaster, or some other path, your resiliance and cunning will be put to the test against deadly foes.

Tombwater is currently in development.

