---
title: Hallow's Eve
author: Max Mraz
splashImg: splash.png
itchioLink: https://maxatrillionator.itch.io/hallows-eve
trailerLink: https://www.youtube.com/embed/uEGRmiMhKn0
screenshots: ['screen1.png', 'screen2.png', 'screen3.png', 'screen4.png', 'screen5.png', 'screen6.png', 'screen7.png', 'screen8.png']
---

On Halloween, the barrier between human world and the spirit world thins. The spirits play tricks, the dead can be seen, and monsters can run their human world errands.

Set in the 90s, play as Ichabod, a pumpkin man who uses his chance to enter the human world to try and rent Jurassic Park from the video store. Unfortunately, the only copy has been rented by a teen who's brought a witch's curse upon the town.

Gameplay is inspired by the classics and the modern classics- a Zeldalike sense of exploration, with the tight, fast combat of Hyper Light Drifter. All soaked in a charming pixel art atmosphere and a dumb sense of humor. Playtime is around 1-2 hours.
