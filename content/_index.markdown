---
title: Home
---

# Moth Atlas
Moth Atlas is a super-indie game studio, just one or two scrappy kids making games with expansive exploration, satisfying combat, and quirky charm.
