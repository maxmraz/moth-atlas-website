'use strict';

document.addEventListener('DOMContentLoaded', () => {
  const clickableImages = Array.from(
    document.querySelectorAll(`[enhance-clickable-image="true"]`)
  ).map((image) => {
    return image.children[0];
  });

  if (clickableImages.length === 0) return;

  // Add an overlay.
  const bodyElement = document.getElementsByTagName('body')[0];
  const overlay = document.createElement('div');
  overlay.classList.add('image-overlay');
  overlay.style.display = 'none';
  bodyElement.appendChild(overlay);
  let currentImageIndex = -1;

  let overlayVisible = false;

  const updateOverlayPosition = () => {
    overlay.style.top = `${window.scrollY}px`;
  };

  const clearOverlay = () => {
    while (overlay.firstChild) {
      overlay.removeChild(overlay.firstChild);
    }
    currentImageIndex = -1;
  };

  const setOverlayVisible = (visible) => {
    if (visible != overlayVisible) {
      overlayVisible = visible;

      if (visible) {
        updateOverlayPosition();
        overlay.style.display = 'flex';
        bodyElement.style.overflow = 'hidden';
      } else {
        overlay.style.display = 'none';
        bodyElement.style.overflow = 'scroll';
        clearOverlay();
      }
    }
  };

  const showImage = (imageIndex, imageUrl) => {
    // Clear current content.
    clearOverlay();

    // Update index.
    currentImageIndex = imageIndex;

    // Show image inside overlay.
    const fullSizeImageElement = document.createElement('img');
    fullSizeImageElement.src = imageUrl;
    overlay.appendChild(fullSizeImageElement);

    // Show overlay.
    setOverlayVisible(true);
  };

  overlay.addEventListener('click', (event) => {
    if (overlayVisible) {
      event.preventDefault();
      if (event.target === overlay) {
        setOverlayVisible(false);
      }
    }
  });

  bodyElement.addEventListener('keydown', (event) => {
    if (!overlayVisible) return;
    if (
      event.key === 'Escape' ||
      event.key == ' ' ||
      event.key == 'Spacebar' ||
      event.key == 'Enter' ||
      event.key == 'Return'
    ) {
      event.preventDefault();
      setOverlayVisible(false);
    } else if (event.key === 'ArrowLeft') {
      event.preventDefault();
      const previousIndex =
        (currentImageIndex - 1 + clickableImages.length) % clickableImages.length;
      const previousImage = clickableImages[previousIndex];
      showImage(previousIndex, previousImage.src);
    } else if (event.key === 'ArrowRight') {
      event.preventDefault();
      const nextIndex = (currentImageIndex + 1) % clickableImages.length;
      const nextImage = clickableImages[nextIndex];
      showImage(nextIndex, nextImage.src);
    }
  });

  window.addEventListener('resize', (_event) => {
    if (overlayVisible) {
      updateOverlayPosition();
    }
  });

  // Events listeners on images.
  for (const [imageIndex, image] of clickableImages.entries()) {
    image.addEventListener('click', (event) => {
      event.preventDefault();
      const imageUrl = image.src;
      showImage(imageIndex, imageUrl);
    });
  }
});
